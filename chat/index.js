var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var nickname;
var users = [];
var usersTest = [];

app.use(express.static('/home/risto/Documents/node/chat'));

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(socket) {
  socket.emit('id', socket.id);
  socket.broadcast.emit('broadcast', 'SERVER: New user connected!');	// Notify users about a new user
  socket.on('disconnect', function() {
    var elementPos = usersTest.map(function(x) {return x.id; }).indexOf(socket.id);	// Iterates over usersTest array of objects to find user who disconnected by their ID
    var objectFound = usersTest[elementPos];
    console.log(usersTest);
    var disconnectedUser = usersTest[elementPos].nickname;
    usersTest.splice(elementPos, 1);	// Remove disconnected user object from usersTest array
    users.splice(disconnectedUser, 1);	// Remove disconnected user from users array

    io.emit('disconnected', disconnectedUser);

    socket.broadcast.emit('broadcast', 'SERVER: ' + disconnectedUser + ' disconnected!');  // Notify users about a disconnected user
  });

  socket.on('nickname', function(nick) {
    usersTest.push( { "id":nick['id'], "nickname":nick['nickname'] } );	// Add new user's ID and nickname to usersTest array
    console.log(usersTest);
    nickn = nick['nickname'];
    socket.broadcast.emit('broadcast', 'SERVER: New user is under nickname: ' + nickn);	// Notify users about new user's nickname
    users.push(nickn);	// Add new user's nickname to users array
    io.emit('users', users);
  });

  socket.on('chat message', function(msg,nickname) {
  	io.emit('chat message', { 'message': msg, 'nickname': nickname });
  });

  socket.on('typing', function(typingUser) {
  	io.emit('typing', typingUser);
  })

  socket.on('stoppedTyping', function(typingUser) {
  	io.emit('stoppedTyping', typingUser);
  })

});

http.listen(3000, function() {
  console.log('listening on *:3000');
});