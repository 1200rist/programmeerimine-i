var socket = io();
var Uid;
var myNickname;
var listedUsers = [];
var alreadyTyping = 0;
var lastTyping;
var scrollbar;



(function($) {
    $.fn.hasScrollBar = function() {
        return this.get(0).scrollHeight > this.height();
    }
})(jQuery);

$(document).ready(function() {
    scrollbar = $('#messages').hasScrollBar();
});

$('#send_message').submit(function() {
    socket.emit('chat message', {
        'message': $('#message-input').val(),
        'nickname': myNickname
    });
    $('#message-input').val('');

    socket.emit('stoppedTyping', myNickname);
    alreadyTyping = 0;

    return false;
});

socket.on('id', function(id) {
    Uid = id;
});

socket.on('chat message', function(data) {
    var scroll_position = check_scroll_position();

    data = data['message'];
    console.log(data);
    var message = data.message;
    var nickname = data.nickname;
    if (nickname == myNickname) {
        $('#messages').append($('<li class="thisUser"><span>'+ message +'</span></li>'));
    } else {
        $('#messages').append($('<li><span><b>'+ nickname + ':</b> ' + message +'</span></li>'));
    }

    check_scroll(scroll_position);
});

socket.on('broadcast', function(info) {
    $('#messages').append($('<li class="broadcast">').text(info));
});

socket.on('newUser', function(newUser) {
    if ($.inArray(newUser, listedUsers) == -1) {
        listedUsers.push(newUser);
        $('aside').append($('<li>').text(newUser));
    }
});

socket.on('users', function(users) {
    console.log(users);
    for (var i = 0; i < users.length; i++) {
        if ($.inArray(users[i], listedUsers) == -1) {
            listedUsers.push(users[i]);
            $('aside').append($('<li>').text(users[i]));
        }
    };
});

socket.on('disconnected', function(disconnectedUser) {
    console.log(disconnectedUser);
    var userTags = document.getElementsByTagName("li");
    for (var i = 0; i < userTags.length; i++) {
        if (userTags[i].textContent == disconnectedUser) {
            $(userTags[i]).remove();
        };
    };
});

$('#nickname_form').submit(function() {
    console.log(Uid);
    socket.emit('nickname', {
        'id': Uid,
        'nickname': $('#nickname').val()
    });
    myNickname = $('#nickname').val();
    $('#form_container').hide();
    return false;
});


function typing() {
    lastTyping = Date.now();
    console.log("Typing...");
    if (!alreadyTyping) {
        socket.emit('typing', myNickname);
        alreadyTyping = 1;
        timeout();
    }
}

function timeout() {
    setTimeout(function() {
        if (lastTyping + 3000 < Date.now()) {
            socket.emit('stoppedTyping', myNickname);
            alreadyTyping = 0;
        } else {
            timeout();
        }
    }, 3500);
}

socket.on('typing', function(typingUser) {
    if (typingUser != myNickname) {
        $('#usersTyping').append($('<li id="' + typingUser + '">').text(typingUser + " is typing..."));
    }
})

socket.on('stoppedTyping', function(typingUser) {
    if (typingUser != myNickname) {
        $('#' + typingUser).remove();
    }
})

function check_scroll(scroll_position) {
    var elem = $('#messages');
    if ( elem.hasScrollBar() && scrollbar == false) {
        elem.scrollTop(elem[0].scrollHeight);
        scrollbar = $('#messages').hasScrollBar();
    } else if ( scrollbar && scroll_position ) {
        elem.scrollTop(elem[0].scrollHeight);
    }
}

function check_scroll_position() {
    var elem = $('#messages');
    if ( elem[0].scrollHeight - elem.scrollTop() == elem.outerHeight() ) {
        return 'bottom';
    } else {
        return false;
    }
}