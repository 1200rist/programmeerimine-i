<?php
	require_once 'includes/access_control.php';
	require_once 'includes/db.php';
	require_once 'includes/functions.php';
?>
<!DOCTYPE html>
<html>
	<head>
		<title>CDH - Central Data Hub</title>
		<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<link rel="stylesheet" type="text/css" href="includes/flatpickr/dist/themes/dark.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="includes/flatpickr/dist/flatpickr.min.js"></script>
		<script src="js/buttons.js"></script>
		<script src="js/modals.js"></script>
		<script src="js/script.js"></script>
		<meta charset="utf-8">
	</head>
	<body>
		<header>
			<a class="home_button" href="/kodutoo-3" alt="home"></a>
			<div class="settings_button"></div>
			<div class="settings_panel">
				<a href="#!">
					<img class="account_icon" src="images/ic_account_circle_white_24px.svg">
					<?php echo $_SESSION['user_name'] ?>
				</a>
				<?php
					if (is_admin()) {
						echo '<a href="user_management.php">User Management</a>';
					}
				?>
				<a href="index.php?logout">Log Out</a>
			</div>
		</header>