class modal {
    constructor(open_button, modal_container) {
        $(open_button).on('click', function(e) {
            modal.open(modal_container);
        });
    }


	static open(modal_container) {
		$(modal_container).show(300);
        $('body').addClass('body--blur');
        $('.modal_container').on('click', modal.check_outside_click);

        $(document).keyup(function(e) {
            if (e.keyCode === 27) {
            	modal.close();
            }
        });

	}


	static check_outside_click(e) {
		if ( !$(e.target).parents('.modal').length && e.target.className != 'modal' ) {
            modal.close();
            $('.modal_container').off();
        }
	}


	static close() {
		$('.modal_container').hide(300);
        $('body').removeClass('body--blur');
	}
} 
