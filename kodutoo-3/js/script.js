$(document).ready(function() {
    
// Settings menu

    var settings_button = new button();
    settings_button.event_handler('.settings_button', 'click', settings_button_action);

    function settings_button_action() {
        $('.settings_panel').slideToggle(200);
        if ( $('.settings_panel').is(':visible') ) {
            $(document).on( 'mouseup', hide_settings_panel );
        };
    };


    function hide_settings_panel(e) {
        var container = $('.settings_panel');

        // if the target of the click isn't the container nor a descendant of the container, nor the settings button
        if (!container.is(e.target) && container.has(e.target).length === 0 && e.target.className != 'settings_button') {
            container.slideUp(200);
            $(document).off( 'mouseup', hide_settings_panel );
        }
    };


// Modals

    // Event modal
    new modal('.add_deadline_button', '.deadline_modal_container');

    // Course modal
    new modal('.add_course_button', '.course_modal_container');

    // Edit course modal
    new modal('.edit_course_button', '.edit_course_modal_container');



// Datepicker  

    $("#deadline_date_input").flatpickr({
        enableTime: true,
        defaultHour: 23,
        defaultMinute: 59,
        altInput: true,
        altFormat: 'Y-m-d H:i:S'
    });


// Ajax calls

    $('#add_deadline_button').on('click', function() {
        var title = $('.modal input[name="deadline_title"]').prop('value');
        var date = $('#deadline_date_input').prop('value');
        var course = $('.modal select[name="course"]').prop('value');
        var description = $('.modal textarea[name="description"]').prop('value');

        $.ajax({
            method: "POST",
            url: "includes/ajax_insert.php",
            data: {
                action: 'add_deadline',
                title: title,
                date: date,
                course: course,
                description: description
            }
        })
        .done(function( msg ) {
            alert( msg );
        });
    });


    $('#add_course_button').on('click', function() {
        var title = $('.modal input[name="course_title"]').prop('value');
        var code = $('.modal input[name="course_code"]').prop('value');
        var lecturer = $('.modal input[name="course_lecturer"]').prop('value');
        var description = $('.modal textarea[name="course_description"]').prop('value');

        var links = [];
        var length = $('.add_link_container').length;
        for (var i = 0; i < length; i++) {
            links.push({
                url: $('.add_link_container').eq([i]).children('input[name="link"]').prop('value'),
                description: $('.add_link_container').eq([i]).children('input[name="link_description"]').prop('value')
            });
        }
        
        $.ajax({
            method: "POST",
            url: "includes/ajax_insert.php",
            data: {
                action: 'add_course',
                title: title,
                code: code,
                lecturer: lecturer,
                description: description,
                links: JSON.stringify(links)
            }
        })
        .done(function( msg ) {
            alert( msg );
        });
    });


    $('#submit_edited_course').on('click', function() {
        var title = $('.modal input[name="course_title"]').prop('value');
        var code = $('.modal input[name="course_code"]').prop('value');
        var lecturer = $('.modal input[name="course_lecturer"]').prop('value');
        var description = $('.modal textarea[name="course_description"]').prop('value');

        var links = [];
        var number_of_links = $('.add_link_container').length;
        for (var i = 0; i < number_of_links; i++) {

            var url = $('.add_link_container').eq([i]).children('input[name="link"]').prop('value');

            if (url.length === 0) {
                continue;
            }

            links.push({
                id: $('.add_link_container').eq([i]).children('input[name="link_id"]').prop('value'),
                url: url,
                description: $('.add_link_container').eq([i]).children('input[name="link_description"]').prop('value')
            });

        }
        
        $.ajax({
            method: "POST",
            url: "includes/ajax_insert.php",
            data: {
                action: 'edit_course',
                title: title,
                code: code,
                lecturer: lecturer,
                description: description,
                links: JSON.stringify(links)
            }
        })
        .done(function( msg ) {
            alert( msg );
        });
    });


    $('#delete_course').on('click', function() {
        var code = $('.modal input[name="course_code"]').prop('value');
        
        $.ajax({
            method: "POST",
            url: "includes/ajax_insert.php",
            data: {
                action: 'delete_course',
                code: code
            }
        })
        .done(function( msg ) {
            alert( msg );
        });
    });


    $('.change_permission').on('click', function(e) {
        var user_id = $(e.target).parent().siblings('.user_id').text();
        var new_permission = e.target.value;

        $.ajax({
            method: "POST",
            url: "includes/ajax_insert.php",
            data: {
                action: 'change_user_permission',
                user_id: user_id,
                new_permission: new_permission
            }
        })
        .done(function( msg ) {
            alert( msg );
        });
    });


// Show/hide floating action buttons

    var mouse_leave_timeout;
    $('.add_content_button').on('mouseover', function(e) {
        clearTimeout(mouse_leave_timeout);
        $('.add_content_plus').addClass('add_content_plus--rotated');
        $('.add_content_buttons').addClass('add_content_buttons--shown');
    });


    $('.add_buttons_container').on('mouseleave', function(e) {
        mouse_leave_timeout = window.setTimeout(function() {
            $('.add_content_plus').removeClass('add_content_plus--rotated');
            $('.add_content_buttons').removeClass('add_content_buttons--shown');
        }, 1000)
    });


// Add another link input to add/edit course modal on click

    $('#add_another_link').on('click', function() {
        $('.add_link_container').first().clone().appendTo('.add_links_container').find('input').val('');
    });
});