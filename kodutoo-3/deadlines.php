<?php
	
	echo '<ul>';

		$deadlines = query( array(
			'columns' => 'course, title, date_time',
			'table'   => 'deadlines'
		));

	    foreach ($deadlines as $deadline) {
	    	$date_time = strtotime($deadline['date_time']);
			$formated_date_time = date('D jS \of F h:i A', $date_time);

			$seconds_to_deadline = $date_time - time();


			// up to 3 days to deadline
			if ($seconds_to_deadline < 259200) {
				$deadline_container_class = 'deadline_content deadline_content--red';
			} else if ($seconds_to_deadline < 432000) {
				$deadline_container_class = 'deadline_content deadline_content--orange';
			} else {
				$deadline_container_class = 'deadline_content';
			}

	    	echo '<li>
	    		<div class="'. $deadline_container_class .'">
		    		<h4>'. $deadline['course'] .' - '. $deadline['title'] .'</h4>
		    		<date>'. $formated_date_time .'</date>
		    	</div>
	    	</li>';
	    }

	echo '</ul>';

?>