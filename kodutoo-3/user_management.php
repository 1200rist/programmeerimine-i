<?php
	require_once 'header.php';
	require_once 'includes/queries.php';
?> 

<div class="user_management_table_container">
	<table>
		<tbody>
			<tr>
				<th>ID</th>
				<th>Username</th>
				<th>Email</th>
				<th>Permission</th>
			</tr>
			<?php get_users() ?>
		</tbody>
	</table>
</div>

<?php require_once('footer.php'); ?>