<?php
	error_reporting(-1); // reports all errors
	ini_set("display_errors", "1"); // shows all errors
	ini_set("log_errors", 1);

	require_once 'db.php';
	require_once 'access_control_2.php';
	require_once 'queries.php';
	require_once 'functions.php';

	if (!can_edit()) {
		exit;
	}

	if ($_POST['action'] == 'add_deadline') {

		$course = $_POST['course'];
		$title = $_POST['title'];
		$description = $_POST['description'];
		$date_time = $_POST['date'];

		insert(array(
			'table' => 'deadlines',
			'columns' => array(
				'course' => $course,
				'title' => $title,
				'description' => $description,
				'date_time' => $date_time
			)
		));

	} else if ($_POST['action'] == 'add_course') {

		$title = $_POST['title'];
		$code = $_POST['code'];
		$lecturer = $_POST['lecturer'];
		$description = $_POST['description'];
		$description = str_replace('<br>', "\n", $description);
		$description = strip_tags($description);
		$links = json_decode($_POST['links']);

		if (empty($title) || empty($code) || empty($lecturer)) {
			die("Some fields are empty");
		}

		insert(array(
			'table' => 'courses',
			'columns' => array(
				'code' => $code,
				'title' => $title,
				'lecturer' => $lecturer,
				'description' => $description
			)
		));


		foreach ($links as $link) {
			insert(array(
				'table' => 'course_links',
				'columns' => array(
					'url' => $link->url,
					'description' => $link->description,
					'course_code' => $code
				)
			));
		}

	} else if ($_POST['action'] == 'edit_course') {

		$title = $_POST['title'];
		$code = $_POST['code'];
		$lecturer = $_POST['lecturer'];
		$description = $_POST['description'];
		$description = str_replace('<br>', "\n", $description);
		$description = strip_tags($description);
		$links = json_decode($_POST['links']);

		if (empty($title) || empty($code) || empty($lecturer)) {
			die("Some fields are empty");
		}

		update(array(
			'table' => 'courses',
			'columns' => array(
				'title' => $title,
				'lecturer' => $lecturer,
				'description' => $description
			),
			'where_column' => 'code',
			'where_condition' => '=',
			'where_value' => $code,
			'limit' => 1
		));

		foreach ($links as $link) {
			update(array(
				'table' => 'course_links',
				'columns' => array(
					'url' => $link->url,
					'description' => $link->description,
					'course_code' => $code
				),
				'where_column' => 'id',
				'where_condition' => '=',
				'where_value' => $link->id,
				'limit' => 1
			));
		}

	} else if ($_POST['action'] == 'delete_course') {

		$code = $_POST['code'];

		delete(array(
			'table' => 'courses',
			'where_column' => 'code',
			'where_condition' => '=',
			'where_value' => $code,
			'limit' => 1
		));

		delete(array(
			'table' => 'course_links',
			'where_column' => 'course_code',
			'where_condition' => '=',
			'where_value' => $code
		));

	}


	if (!is_admin()) {
		$conn = null;
		exit;
	}
	
	if ($_POST['action'] == 'change_user_permission') {

		$user_id = $_POST['user_id'];
		$new_permission = $_POST['new_permission'];

		if (empty($user_id) || ($new_permission != 'viewer' && $new_permission != 'editor' && $new_permission != 'admin')) {
			die("ERROR, couldn't update permission");
		}

		update(array(
			'table' => 'users',
			'columns' => array(
				'permission' => $new_permission
			),
			'where_column' => 'user_id',
			'where_condition' => '=',
			'where_value' => $user_id,
			'limit' => 1
		));
	}

	$conn = null;
?>