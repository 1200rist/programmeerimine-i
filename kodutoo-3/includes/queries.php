<?php
	function query(array $args) {
		global $conn;

		try {
			if (!empty($args['where_column']) && !empty($args['where_condition']) && !empty($args['where_value'])) {
				$where_clause = " WHERE ". $args['where_column'] ." ". $args['where_condition'] ." '". $args['where_value'] ."'";
			}

			if (!empty($args['limit'])) {
				$limit = " LIMIT ". $args['limit'];
			}

		    $sql = "SELECT ". $args['columns'] ." FROM ". $args['table'] . $where_clause . $limit;

		    $sth = $conn->prepare($sql);
		    $sth->execute();
		    $sth->setFetchMode(PDO::FETCH_ASSOC);

		    if ($args['limit'] === 1) {
		    	$results = $sth->fetch();
		    } else {
		    	$results = $sth->fetchAll();
		    }

		    return $results;
		}

		catch(PDOException $e) {
		    return $sql . "<br>" . $e->getMessage();
		}
	}

	function update(array $args) {
		global $conn;

		$table = $args['table'];
		$where_column = $args['where_column'];
		$where_condition = $args['where_condition'];
		$where_value = $args['where_value'];

		$columns = implode(',', array_keys($args['columns']));
		$prefixed_columns = ':'. implode(',:', array_keys($args['columns']));
		$set = '';

		if (!empty($args['limit'])) {
			$limit = " LIMIT ". $args['limit'];
		}

		foreach ($args['columns'] as $key => $val) {
		    $set .= $key .' = :'. $key .',';
		}

		$set = rtrim($set, ',');


		try {
			$sth = $conn->prepare('UPDATE '. $table .' SET '. $set .' WHERE '. $where_column .' '. $where_condition .' :'. $where_column . $limit);

			$sth->bindParam(':'. $where_column, $where_value);

		    foreach ($args['columns'] as $key => &$val) {
			    $sth->bindParam(':'. $key, $val);
			}

			$sth->execute();
		}
		catch(PDOException $e) {
		    echo $sql . "<br>" . $e->getMessage();
		}
	}

	function insert(array $args) {
		global $conn;

		$table = $args['table'];
		$columns = implode(',', array_keys($args['columns']));
		$prefixed_columns = ':'. implode(',:', array_keys($args['columns']));
		

		try {
			$sth = $conn->prepare('INSERT INTO '. $table .' ('. $columns .') VALUES ('. $prefixed_columns .')');

		    foreach ($args['columns'] as $key => &$val) {
			    $sth->bindParam(':'. $key, $val);
			}

			$sth->execute();
		}
		catch(PDOException $e) {
		    echo $sql . "<br>" . $e->getMessage();
		}
	}


	function delete(array $args) {
		global $conn;

		$table = $args['table'];
		$where_column = $args['where_column'];
		$where_condition = $args['where_condition'];
		$where_value = $args['where_value'];

		if (!empty($args['limit'])) {
			$limit = " LIMIT ". $args['limit'];
		}
		

		try {
			$sth = $conn->prepare('DELETE FROM '. $table .' WHERE '. $where_column .' '. $where_condition .' :'. $where_column . $limit);

			$sth->bindParam(':'. $where_column, $where_value);

			$sth->execute();
		}
		catch(PDOException $e) {
		    echo $sql . "<br>" . $e->getMessage();
		}
	}

?>