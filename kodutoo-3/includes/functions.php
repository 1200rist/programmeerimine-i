<?php

	function format_urls_in_text($text){
	    $reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
	    preg_match_all($reg_exUrl, $text, $matches);
	    $usedPatterns = array();
	    foreach($matches[0] as $pattern){
	        if(!array_key_exists($pattern, $usedPatterns)){
	            $usedPatterns[$pattern]=true;
	            $text = str_replace($pattern, '<a href="'. $pattern .'">'. $pattern .'</a>', $text);
	        }
	    }
	    return $text;
	}


	function get_courses() {
		$courses = query( array(
			'columns' => 'code, title, lecturer',
			'table'   => 'courses'
		));

		foreach ($courses as $course) {
	    	echo '<div>
	    		<a href="course_detailed.php?course='. $course['code'] .'">
		    		<h3>'. $course['title'] .'</h3>
		    		<h5>'. $course['lecturer'] .'</h5>
		    		<span>'. $course['code'] .'</span>
	    		</a>
	    	</div>';
	    }

	    return $courses;
	}


	function get_course_details($code) {
		$course = query( array(
			'columns' => 'title, lecturer, description',
			'table' => 'courses',
			'where_column' => 'code',
			'where_condition' => '=',
			'where_value' => $code,
			'limit' => 1
		));

		echo '<div class="course_details">';

			$links = query( array(
				'columns' => 'id, url, description',
				'table' => 'course_links',
				'where_column' => 'course_code',
				'where_condition' => '=',
				'where_value' => $code
			));

		    echo !empty($links) ? '<div class="links">' : '';

		    foreach ($links as $link) {
		    	echo '<div class="link_button_container">
					<a href="'. $link['url'] .'">
						<img src="https://www.google.com/s2/favicons?domain='. $link['url'] .'" alt="'. $link['description'] .'">
						<span>'. $link['description'] .'</span>
					</a>
		    	</div>';
		    }

			echo !empty($links) ? '</div>' : '';

			echo '<div class="course_content">
	    		<h3>'. $course['title'] .' ('. $code .')</h3>
	    		<h4>'. $course['lecturer'] .'</h4>
	    		<p>'. nl2br(format_urls_in_text($course['description'])) .'</p>
	    	</div>
		</div>';

	    return array(
	    	'course' => $course,
	    	'links' => $links
	    );
	}


	function get_users() {
		$users = query( array(
			'columns' => 'user_id, user_name, user_email, permission',
			'table'   => 'users'
		));

		foreach ($users as $user) {
	    	echo '<tr>
	    		<td class="user_id">'. $user['user_id'] .'</td>
	    		<td class="user_name">'. $user['user_name'] .'</td>
	    		<td class="user_email">'. $user['user_email'] .'</td>
	    		<td class="permission">'. $user['permission'] .'</td>
	    		<td>';
	    			switch ($user['permission']) {
	    				case 'viewer':
	    					echo '<button class="change_permission" value="editor">Promote to editor</button>
	    						<button class="change_permission" value="admin">Promote to admin</button>';
	    					break;

	    				case 'editor':
	    					echo '<button class="change_permission" value="viewer">Demote to viewer</button>
	    						<button class="change_permission" value="admin">Promote to admin</button>';
	    					break;

	    				case 'admin':
	    					echo '<button class="change_permission" value="editor">Demote to editor</button>
	    						<button class="change_permission" value="viewer">Demote to viewer</button>';
	    					break;
	    			}
	    		echo '</td>
	    	</tr>';
	    }
	}


	function can_edit() {
		if ($_SESSION['user_permission'] == 'admin' || $_SESSION['user_permission'] == 'editor') {
			return true;
		} else {
			return false;
		}
	}


	function is_admin() {
		if ($_SESSION['user_permission'] == 'admin') {
			return true;
		} else {
			return false;
		}
	}

?>