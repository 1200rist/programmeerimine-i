<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/style.css" rel="stylesheet" media="screen">
  </head>

  <body class="login_body">
    <div class="container">
        <?php
            // show potential errors / feedback (from login object)
            if (isset($login)) {
                if ($login->errors) {
                    foreach ($login->errors as $error) {
                        echo $error;
                    }
                }
                if ($login->messages) {
                    foreach ($login->messages as $message) {
                        echo $message;
                    }
                }
            }
        ?>
        <!-- login form box -->
        <form method="post" action="index.php" name="loginform">

            <label for="login_input_username">Username</label>
            <input id="login_input_username" class="login_input" type="text" name="user_name" required />

            <label for="login_input_password">Password</label>
            <input id="login_input_password" class="login_input" type="password" name="user_password" autocomplete="off" required />

            <input type="submit"  name="login" value="Log in" />

        </form>

        <a href="includes/php-login-minimal/register.php">Register new account</a>
    </div> <!-- /container -->

    <a class="image_credit" href="http://www.freepik.com">Designed by mrsiraphol / Freepik</a>
  </body>
</html>
<?php exit() ?>