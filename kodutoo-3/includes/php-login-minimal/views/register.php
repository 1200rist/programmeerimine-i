<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Register</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="../../css/style.css" rel="stylesheet" media="screen">
  </head>

  <body class="login_body">
    <div class="container">
        <?php
            // show potential errors / feedback (from registration object)
            if (isset($registration)) {
                if ($registration->errors) {
                    foreach ($registration->errors as $error) {
                        echo $error;
                    }
                }
                if ($registration->messages) {
                    foreach ($registration->messages as $message) {
                        echo $message;
                    }
                }
            }
        ?>
        <h1>Register</h1>
        <!-- register form -->
        <form method="post" action="register.php" name="registerform">

            <!-- the user name input field uses a HTML5 pattern check -->
            <label for="login_input_username">Username (only letters and numbers, 2 to 64 characters)</label>
            <input id="login_input_username" class="login_input" type="text" pattern="[a-zA-Z0-9]{2,64}" name="user_name" required />

            <!-- the email input field uses a HTML5 email type check -->
            <label for="login_input_email">User's email</label>
            <input id="login_input_email" class="login_input" type="email" name="user_email" required />

            <label for="login_input_password_new">Password (min. 6 characters)</label>
            <input id="login_input_password_new" class="login_input" type="password" name="user_password_new" pattern=".{6,}" required autocomplete="off" />

            <label for="login_input_password_repeat">Repeat password</label>
            <input id="login_input_password_repeat" class="login_input" type="password" name="user_password_repeat" pattern=".{6,}" required autocomplete="off" />
            <input type="submit"  name="register" value="Register" />

        </form>

        <!-- backlink -->
        <a href="index.php">Back to Login Page</a>
    </div> <!-- /container -->

    <a class="image_credit" href="http://www.freepik.com">Designed by mrsiraphol / Freepik</a>
  </body>
</html>
<?php exit() ?>