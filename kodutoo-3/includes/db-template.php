<?php

	/**
 	* Configuration for: Database Connection
	*
	* servername: database host, usually it's "127.0.0.1" or "localhost", some servers also need port info
	* db_name: name of the database. please note: database and database table are not the same thing
	* username: user for your database. the user needs to have rights for SELECT, UPDATE, DELETE and INSERT.
	* password: the password of the above user

	* Rename this to db.php
	*/

	$servername = "";
	$username   = "";
	$password   = "";
	$db_name = "";

	try {
	    $conn = new PDO("mysql:host=$servername;dbname=$db_name;charset=utf8", $username, $password);

	    // set the PDO error mode to exception
	    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	} catch (PDOException $e) {
	    echo "Connection failed: " . $e->getMessage();
	}
?>