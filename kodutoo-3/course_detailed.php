<?php
	require_once 'header.php';
	require_once 'includes/queries.php';
	$code = $_GET['course'];
?>

<div class="main-container">
	<aside class="deadlines">
		<h2>Tähtajad</h2>
		<?php include_once 'deadlines.php' ?>
	</aside>
	<main>
		<?php
			$course_details = get_course_details($code);
			$course = $course_details['course'];
			$links = $course_details['links'];
		?>
	</main>

	<?php if (can_edit()) { ?>
		<div class="edit_button edit_course_button">
			<span class="edit_icon"></span>
		</div>
	<?php } ?>
</div>

<?php if (can_edit()) { ?>
	<div class="modal_container edit_course_modal_container">
		<div class="modal">
			<h2>Muuda ainet</h2>

			<input type="text" name="course_title" placeholder="Pealkiri" required value="<?php echo $course['title'] ?>">
			<input type="hidden" name="course_code" required value="<?php echo $code ?>">
			<input type="text" name="course_lecturer" placeholder="Õppejõud" required value="<?php echo $course['lecturer'] ?>">
			<textarea name="course_description" placeholder="Kirjeldus/ lisa informatsioon"><?php echo $course['description'] ?></textarea>
			<div class="add_links_container">
				<h3>Lingid</h3>

				<?php
					foreach ($links as $link) {
						echo '<div class="add_link_container">
							<input type="hidden" name="link_id" value="'. $link['id'] .'">
							<input type="text" name="link" placeholder="URL" value="'. $link['url'] .'">
							<input type="text" name="link_description" placeholder="Selgitus" value="'. $link['description'] .'">
						</div>';
					}
				?>

				<div class="add_link_container">
					<input type="hidden" name="link_id">
					<input type="text" name="link" placeholder="URL">
					<input type="text" name="link_description" placeholder="Selgitus">
				</div>
				
				<div id="add_another_link"></div>
			</div>

			<div class="modal_buttons_container">
				<button class="delete_button" id="delete_course">Kustuta aine</button>
				<button class="send_button" id="submit_edited_course">Salvesta muudatused</button>
			</div>
		</div>
	</div>
<?php } ?>

<?php require_once 'footer.php'; ?>