<?php
	require_once 'header.php';
	require_once 'includes/queries.php';
?>

<div class="main-container">
	<aside class="deadlines">
		<h2>Tähtajad</h2>
		<?php include_once 'deadlines.php' ?>
	</aside>
	<main>
		<div class="grid-container">
			<?php $courses = get_courses() ?>
		</div>
	</main>
</div>

<?php if (can_edit()) { ?>
	<div class="add_buttons_container">
		<div class="add_content_buttons">

			<div class="add_deadline_button">
				<span class="add_deadline_icon"></span>
				<span class="add_button_text">Lisa tähtaeg</span>
			</div>

			<div class="add_course_button">
				<span class="add_course_icon"></span>
				<span class="add_button_text">Lisa aine</span>
			</div>

		</div>

		<div class="add_content_button">
			<span class="add_content_plus"></span>
		</div>
	</div>

	<div class="modal_container deadline_modal_container">
		<div class="modal">
			<h2>Lisa uus tähtaeg</h2>

			<input type="text" name="deadline_title" placeholder="Pealkiri">
			<input type="text" id="deadline_date_input" name="deadline_date" placeholder="Tähtaeg">

			<select name="course">
				<option default class="option--hidden">Aine</option>
				<?php
					foreach ($courses as $course) {
				    	echo '<option>'. $course['title'] .' ('. $course['lecturer'] .')</option>';
					}
				?>
			</select>

			<textarea name="description" placeholder="Kirjeldus"></textarea>

			<button class="send_button" id="add_deadline_button">Lisa tähtaeg</button>
		</div>
	</div>

	<div class="modal_container course_modal_container">
		<div class="modal">
			<h2>Lisa uus aine</h2>

			<input type="text" name="course_title" placeholder="Pealkiri" required>
			<input type="text" name="course_code" placeholder="Ainekood" required>
			<input type="text" name="course_lecturer" placeholder="Õppejõud" required>
			<textarea name="course_description" placeholder="Kirjeldus/ lisa informatsioon"></textarea>
			<div class="add_links_container">
				<h3>Lingid</h3>
				<div class="add_link_container">
					<input type="text" name="link" placeholder="URL">
					<input type="text" name="link_description" placeholder="Selgitus">
				</div>
				<div id="add_another_link"></div>
			</div>

			<button class="send_button" id="add_course_button">Lisa aine</button>
		</div>
	</div>
<?php } ?>

<?php require_once('footer.php'); ?>